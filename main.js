//delete all datasets, key-value, requests Q with options to exclude needed values

const ApifyClient = require('apify-client');

//add needed data here
const apifyClient = new ApifyClient({
  userId: '',
  token: ''
});

// add here manually ids for each item to ignore during removing like exclude: ['dfksadlf', 'dfrjsdlkfsd']
let clearOptions = {
  datasets: {
    exclude: []
  },
  keyValueStores: {
    exclude: []
  },
  requestQueues: {
    exclude: []
  }
};

let config = {
  datasets: {
    init:{
      method: 'datasets'
    },
    get: {
      method: 'listDatasets',
      options: {unnamed: true}
    },
    delete: {
      method:'deleteDataset',
      options: {key: 'datasetId'}
    }
  },
  keyValueStores:{
    init:{
      method: 'keyValueStores'
    },
    get: {
      method: 'listStores',
      options: {unnamed: true}
    },
    delete: {
      method:'deleteStore',
      options: {key: 'storeId'}
    }
  },
  requestQueues:{
    init:{
      method: 'requestQueues'
    },
    get: {
      method: 'listQueues',
      options: {unnamed: true}
    },
    delete: {
      method:'deleteQueue',
      options: {key: 'queueId'}
    }
  }
};

clearItemsStore()
  .then(res => console.log(res));

async function clearItemsStore(items){
  let key, res = {};

  for (key in config){
    console.log(key, config[key]);
    let tempRes = await clearItemStore(key, config[key], clearOptions[key]['exclude']);
    res = Object.assign({}, res , {[key]: tempRes});
  }

  return res;
}

async function clearItemStore(key, config, ignoredItems){
  const items = apifyClient[config.init.method];
  let listItemsObj = {};
  let listItemsArray = await items[config.get.method](config.get.options);


  listItemsArray.items.forEach(item => {
    listItemsObj = Object.assign({}, listItemsObj, {[item.id]: item})
  });

  let itemsIds = Object.keys(listItemsObj);


  ignoredItems.forEach(excludeItem => {
    let excludeItemIndex = itemsIds.indexOf(excludeItem);
    if (excludeItemIndex !== -1) {
      itemsIds.splice(excludeItemIndex, 1);
    }
  });
  try {
    itemsIds.forEach(async (item) => {
      await items[config.delete.method]({[config.delete.options.key]: item})
    });
    return true
  } catch (e) {
    console.log(e);
    return false
  }
}
